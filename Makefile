# (C) 2004, 2005, 2008, 2010, 2017 W. Martin Borgert <debacle@debian.org>
# See COPYING for the license status of this software.

# You need to install the build dependencies ('apt-get build-dep refcard'):
# docbook-xsl texlive-extra-utils pdftk po4a xmlroff dblatex poppler-utils xsltproc

# The "--keep 0" can be removed when the translations are ready
TRANSLATE=po4a-translate --format docbook --keep 0
UPDATEPO=po4a-updatepo --format docbook
XP=xsltproc --nonet --novalid
XMLROFF=xmlroff --backend cairo --continue
DIA=dia

PDFJOIN=pdfjoin --vanilla
PDFNUP=pdfnup --vanilla

# Not yet implemted in the new version (neither FO nor dblatex)
#ifneq ($(AD),)
#    ad=--stringparam ad $(AD)
#else
#    ad=
#endif

# space separated list of languages to ignore
DISABLE=ml
PO_FILES=$(sort $(filter-out $(patsubst %,po4a/%.po,$(DISABLE)), $(wildcard po4a/*.po)))
# international standard format
A4_LANGS=en $(patsubst po4a/%.po,%,$(PO_FILES))
# non-standard format for north america
LT_LANGS=en es fr

# Because of current limitations of xmlroff in respect to layout, we
# can also use dblatex. Some languages don't work well:
# See #492365 (he), #492366 (el), #492350 (ja), # #482857 (zh_CN)
XMLROFF_LANGS=$(A4_LANGS)
DBLATEX=dblatex --backend=xetex --texstyle=refcard.sty --xsl-user=dblatex.xsl
ifdef USE_DBLATEX
    ifeq ($(USE_DBLATEX),2)
	XMLROFF_LANGS=
    else
	XMLROFF_LANGS=ar he ml
    endif
else
    XMLROFF_LANGS=$(A4_LANGS)
endif

ENTRIES=$(patsubst %,entries-%.dbk,$(A4_LANGS))

PDFS=$(patsubst %,refcard-%-a4.pdf,$(A4_LANGS)) \
    $(patsubst %,refcard-%-lt.pdf,$(LT_LANGS))

refcard-%.dbk: entries-%.dbk preproc.xsl refcard.dbk
	$(XP) preproc.xsl $< > $@

refcard-%-a4.fo: refcard-%.dbk fo.xsl
	$(XP) fo.xsl $< > $@

refcard-%-lt.fo: refcard-%.dbk fo.xsl
	$(XP) --stringparam page.height 216mm \
	    --stringparam page.width 93mm fo.xsl $< > $@

# the .fo is not used for dblatex, but I don't care
refcard-%.s.pdf: refcard-%.fo dblatex.xsl refcard.sty
	L=$$(echo $@ | sed 's/.*-\(.*\)-.*/\1/'); \
	USE_XR=$$(echo " $(XMLROFF_LANGS) " | grep " $$L "); \
	if [ -n "$$USE_XR" ]; then \
	    $(XMLROFF) -o $@ $<; \
	else \
	    if [ -n "$$USE_DBLATEX" ]; then \
		SRC=$$(echo $<|sed 's/-\(a4\|lt\).fo/.dbk/'); \
		FMT=$$(echo $<|sed 's/.*-\(a4\|lt\).fo/\1/'); \
		$(DBLATEX) --output=$@ --param=format=$$FMT $$SRC; \
	    fi; \
	fi

refcard-%-a4.t.pdf: refcard-%-a4.s.pdf empty.pdf
	$(PDFJOIN) --fitpaper true --outfile $<.x.pdf $< empty.pdf empty.pdf
	$(PDFNUP) --nup 3x1 --outfile $@ --paper a4paper $<.x.pdf 5-6,1-4
	rm $<.x.pdf

refcard-%-lt.t.pdf: refcard-%-lt.s.pdf empty.pdf
	$(PDFJOIN) --fitpaper true --outfile $<.x.pdf $< empty.pdf empty.pdf
	$(PDFNUP) --nup 3x1 --outfile $@ --paper letterpaper $<.x.pdf 5-6,1-4
	rm $<.x.pdf

refcard-%.pdf: refcard-%.t.pdf metadata.xsl
	L=$$(echo $@ | sed 's/.*-\(.*\)-.*/\1/'); \
	$(XP) metadata.xsl entries-$$L.dbk | \
	pdftk $< update_info_utf8 - output $@

entries-%.dbk: po4a/%.po entries.dbk
	$(TRANSLATE) -m entries.dbk -p $< -l $@

all: index.html refcard.png $(PDFS) check

updatepo: $(PO_FILES) po4a/entries.pot
$(PO_FILES) po4a/entries.pot: entries.dbk
	$(UPDATEPO) --master $< --po $@

tidypo:
	for po in $(PO_FILES) po4a/entries.pot; do \
	    msgcat $$po > $$po.tmp && mv $$po.tmp $$po; \
	done

# make sure, we have always at least six pages for pdfnup
empty.fo: empty.dbk fo.xsl
	$(XP) fo.xsl $< > $@

empty.pdf: empty.fo
	$(XMLROFF) -o $@ $<

.PHONY: all clean count mrproper

entries-en.dbk: entries.dbk
	cp -f $< $@

statistics.xml: $(PO_FILES) Makefile
	( \
	    LANG=C; export LANG; \
	    echo '<?xml version="1.0" encoding="utf-8"?>'; \
	    echo '<statistics>'; \
	    for po in $(PO_FILES); do \
		L=$$(basename $$po .po); \
		echo -n '<s l="'$$L'">'; \
		msgfmt --statistics $$po 2>&1 | \
		    sed -e 's|\([0-9]*\) translated messages*|\1\&#x00A0;ok|' \
			-e 's|\([0-9]*\) fuzzy translations*|\1\&#x00A0;fuzzy|' \
			-e 's|\([0-9]*\) untranslated messages*|\1\&#x00A0;todo|' \
			-e 's|$$|</s>|'; \
		grep '^"Last-Translator:' $$po | \
		    sed -e 's/</\&lt;/g' -e 's/>/\&gt;/g' | \
		    sed 's,"Last-Translator: *\(.*\)\\n.*,<t l="'$$L'">\1</t>,'; \
	    done; \
	    echo '</statistics>'; \
	) > $@

statistics.txt: statistics.xml
	cat $< | grep '^<s ' | sed -e 's/<s l="//' -e 's/">/	/' \
	    -e 's/&#x00A0;/ /g' -e 's/<\/s>//' > $@

index.html: refcard.dbk $(ENTRIES) statistics.xml html.xsl
	$(XP) --xinclude html.xsl $< > $@

refcard.png: refcard.dia
	$(DIA) --export=$@ $<

checkpo:
	@for po in $(PO_FILES); do \
	    echo $$po; \
	    msgfmt --check --verbose $$po; \
	done

check:
	@for p in $(PDFS); do \
	    if [ \! -e $$p ]; then \
	        echo "ERROR: $$p does not exist!" 1>&2 ||:; \
		continue; \
            fi; \
	    PAGES=$$(pdfinfo "$$p" | grep ^Pages: | sed 's/Pages:[^0-9]*//'); \
	    [ "$$PAGES" -ne 2 ] && \
	    echo "ERROR: $$p has $$PAGES pages instead of 2!" 1>&2 ||:; \
	done

count:
	@for d in $(ENTRIES); do \
	    IDS=$$(grep '<glossentry id=' $$d | wc -l); \
	    echo $$IDS	$$d; \
	done | sort -nr

clean:
	-rm -f *~ refcard-*.dbk refcard-*.fo refcard-*.[st].pdf \
	    $(ENTRIES) statistics.xml statistics.txt messages.mo \
	    empty.fo empty.pdf

mrproper: clean
	-rm -f index.html refcard.png $(PDFS) entries-*.dbk
